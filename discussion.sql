-- To login to MySQL
mysql -u root - p

-- To Show the list of databases
SHOW DATABASES;

-- For creating a new database
CREATE DATABASE db_name;

-- For switching the active db
USE db_name;

-- Creating tables in db
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT, -- always part of creating table because MySQL does not create automatically an id like mongoDB does
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id) -- we set the id as the primary key 
);

CREATE TABLE artist (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	data_released DATE NOT NULL,
	artist_id INT NOT NULL, -- is a foreign key, thus we put CONSTRAINT below
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artist(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- For creating tables with a foreign key, we use the CONSTRAINT kweyword where we can set the local property which serve as the foreign key that is connected to a different table. Within the CONSTRAINT keyword, we also declare the behavior of the property when it is updated or deleted.

-- CASCADE means the foreign key property will update if the id of the table that it is referencing is also updated

-- RESTRICT means the opposite of cascade wherein in wont update or do anything to the foreign ket property once the table that it is referencing is deleted.

CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	data_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artist(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);